﻿﻿using System;
using System.Data.SqlClient;
﻿using System.Diagnostics;

/// <summary>
/// DataConn.cs
/// 03/01/2012
/// Darren Whitfield
///
/// This will provide a global method to connect to a database via details on a config file.
/// When Connect() is called, it will connect to the database, and return any data, all at once.
///
/// Disconnect() will disconnect the application for the database, and destroy any data, maintaining security.
/// </summary>
///

public class DataConn
{
    private static SqlConnection _sqlConn;

    [DebuggerDisplay("Sql Server={SqlServer}")]
    public static string SqlServer { get; set; }

    [DebuggerDisplay("UserName={UserName}")]
    public static string UserName { get; set; }

    [DebuggerDisplay("Password={Password}")]
    public static string Password { get; set; }

    [DebuggerDisplay("Database={Database}")]
    public static string Database { get; set; }

    /// <summary>
    /// This will connect the application to the database, and return any further data.
    ///
    /// You dont need to call Connect(), then datastring(connStr, Connect()),
    /// as the connect() in the data string is all you need.
    /// If you proceed to call Connect();, you will get a pooling error.
    /// </summary>
    /// <returns>
    /// _sqlConn as SqlConnection
    /// </returns>
    public static SqlConnection Connect()
    {
        //Initialise the connection to the server using the connection string.
        _sqlConn = SqlConnection();

        try
        {
            //Open the connection, we do this here so we can instantly be able to use SQL commands in the code.
            _sqlConn.Open();

            return _sqlConn;
        }
        catch (Exception)
        {
            return null;
        }

    }

    /// <summary>
    /// Returns the current SQLConnection
    /// </summary>
    /// <returns></returns>
    public static SqlConnection SqlConnection()
    {
        return new SqlConnection(ConnectionString());
    }

    /// <summary>
    /// Returns the current ConnectionString
    /// </summary>
    /// <returns></returns>
    public static string ConnectionString()
    {
        return String.Format(@"SERVER={0}; UID={1}; pwd={2}; Database={3}", SqlServer, UserName, Password, Database);
    }

    /// <summary>
    /// Disconnects the Application from the database, and clears any other data.
    /// </summary>
    public static void Disconnect()
    {
        _sqlConn.Dispose();
        _sqlConn.Close();
    }
}