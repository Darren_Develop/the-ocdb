namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatedLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TypeNotifications", "tn_Name", c => c.String(nullable: false, maxLength: 26, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TypeNotifications", "tn_Name", c => c.String(nullable: false, maxLength: 10, unicode: false));
        }
    }
}
