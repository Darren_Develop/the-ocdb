namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IncreasedMaxMessageSize : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.UserMail", "um_Message", c => c.String(maxLength: 2000, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserMail", "um_Message", c => c.String(maxLength: 500, unicode: false));
        }
    }
}
