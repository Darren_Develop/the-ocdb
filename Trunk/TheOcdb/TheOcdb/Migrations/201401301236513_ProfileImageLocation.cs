namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProfileImageLocation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DbUsers", "u_ProfileImage", c => c.String(maxLength: 250, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DbUsers", "u_ProfileImage");
        }
    }
}
