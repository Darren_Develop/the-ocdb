namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPersistantFlag : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteNotifications", "sn_Persistant", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SiteNotifications", "sn_Persistant");
        }
    }
}
