namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initiatingMail1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.UserMail", "u_GUID_From");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserMail", "u_GUID_From", c => c.String(maxLength: 50, unicode: false));
        }
    }
}
