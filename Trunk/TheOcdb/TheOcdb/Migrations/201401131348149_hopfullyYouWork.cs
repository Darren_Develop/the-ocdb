namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class hopfullyYouWork : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SiteNotifications", "sn_Type", c => c.Int(nullable: false));
            AddColumn("dbo.SiteNotifications", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.SiteNotifications", "NotificationType_tn_ID", c => c.Int());
            CreateIndex("dbo.SiteNotifications", "NotificationType_tn_ID");
            AddForeignKey("dbo.SiteNotifications", "NotificationType_tn_ID", "dbo.TypeNotifications", "tn_ID");
            DropColumn("dbo.SiteNotifications", "sn_TypeID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SiteNotifications", "sn_TypeID", c => c.Int(nullable: false));
            DropForeignKey("dbo.SiteNotifications", "NotificationType_tn_ID", "dbo.TypeNotifications");
            DropIndex("dbo.SiteNotifications", new[] { "NotificationType_tn_ID" });
            DropColumn("dbo.SiteNotifications", "NotificationType_tn_ID");
            DropColumn("dbo.SiteNotifications", "Discriminator");
            DropColumn("dbo.SiteNotifications", "sn_Type");
        }
    }
}
