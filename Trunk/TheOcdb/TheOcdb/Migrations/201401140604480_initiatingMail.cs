namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initiatingMail : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.SiteNotifications", newName: "UserNotifications");
            CreateTable(
                "dbo.UserContactList",
                c => new
                    {
                        uc_ID = c.Int(nullable: false, identity: true),
                        u_ID_Current = c.Int(nullable: false),
                        u_ID_Contact = c.Int(),
                        u_GUID_Contact = c.String(nullable: false, maxLength: 50, unicode: false),
                        uc_SetName = c.String(maxLength: 50, unicode: false),
                        uc_DateAdded = c.DateTime(nullable: false),
                        uc_Removed = c.Byte(),
                        uc_DateRemoved = c.DateTime(),
                    })
                .PrimaryKey(t => t.uc_ID);
            
            CreateTable(
                "dbo.UserMail",
                c => new
                    {
                        um_ID = c.Int(nullable: false, identity: true),
                        u_ID_From = c.Int(nullable: false),
                        u_GUID_From = c.String(maxLength: 50, unicode: false),
                        u_ID_To = c.Int(nullable: false),
                        um_DateSent = c.DateTime(nullable: false),
                        um_Subject = c.String(maxLength: 50, unicode: false),
                        um_Message = c.String(maxLength: 500, unicode: false),
                        um_MessageRead = c.Byte(nullable: false),
                        um_Removed = c.Byte(),
                        um_RemovedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.um_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserMail");
            DropTable("dbo.UserContactList");
            RenameTable(name: "dbo.UserNotifications", newName: "SiteNotifications");
        }
    }
}
