namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Connection",
                c => new
                    {
                        conn_ID = c.Int(nullable: false, identity: true),
                        ConnectionID = c.String(maxLength: 40, unicode: false),
                        UserAgent = c.String(maxLength: 200, unicode: false),
                        Connected = c.Boolean(nullable: false),
                        DbUsers_u_ID = c.Int(),
                    })
                .PrimaryKey(t => t.conn_ID)
                .ForeignKey("dbo.DbUsers", t => t.DbUsers_u_ID)
                .Index(t => t.DbUsers_u_ID);
            
            CreateTable(
                "dbo.DbUsers",
                c => new
                    {
                        u_ID = c.Int(nullable: false, identity: true),
                        u_LoginName = c.String(nullable: false, maxLength: 50, unicode: false),
                        u_LoginPassword = c.String(nullable: false, maxLength: 1000, unicode: false),
                        u_Name = c.String(maxLength: 50, unicode: false),
                        u_Surname = c.String(maxLength: 50, unicode: false),
                        u_Email = c.String(nullable: false, maxLength: 50, unicode: false),
                        u_DateAdded = c.DateTime(nullable: false),
                        u_ArticlePassword = c.String(maxLength: 1000, unicode: false),
                        u_Removed = c.Byte(nullable: false),
                        u_RemainingInvites = c.Int(nullable: false),
                        ss_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.u_ID);
            
            CreateTable(
                "dbo.GlobalOptions",
                c => new
                    {
                        co_ID = c.Int(nullable: false, identity: true),
                        co_Name = c.String(nullable: false, maxLength: 100, unicode: false),
                        co_Value = c.String(nullable: false, maxLength: 500, unicode: false),
                        co_DateChanged = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.co_ID);
            
            CreateTable(
                "dbo.SiteFunctions",
                c => new
                    {
                        sf_ID = c.Int(nullable: false, identity: true),
                        sf_Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        sf_DateChanged = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.sf_ID);
            
            CreateTable(
                "dbo.SiteInvites",
                c => new
                    {
                        i_ID = c.Int(nullable: false, identity: true),
                        i_Key = c.String(nullable: false, maxLength: 50, unicode: false),
                        i_DateAdded = c.DateTime(nullable: false),
                        i_Removed = c.Byte(nullable: false),
                        i_Used = c.Byte(nullable: false),
                        i_DateUsed = c.DateTime(nullable: false),
                        u_ID_Sent = c.Int(nullable: false),
                        u_ID_Used = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.i_ID);
            
            CreateTable(
                "dbo.UserLogins",
                c => new
                    {
                        su_ID = c.Int(nullable: false, identity: true),
                        su_Hash = c.String(nullable: false, maxLength: 50, unicode: false),
                        su_DateAdded = c.DateTime(nullable: false),
                        su_ExpiryDate = c.DateTime(nullable: false),
                        su_Removed = c.Byte(nullable: false),
                        u_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.su_ID);
            
            CreateTable(
                "dbo.UserRolls",
                c => new
                    {
                        ss_ID = c.Int(nullable: false, identity: true),
                        ss_AccessLevel = c.Int(nullable: false),
                        ss_AccessName = c.String(nullable: false, maxLength: 50, unicode: false),
                        ss_DerivedID = c.Int(nullable: false),
                        ss_Removed = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => t.ss_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Connection", "DbUsers_u_ID", "dbo.DbUsers");
            DropIndex("dbo.Connection", new[] { "DbUsers_u_ID" });
            DropTable("dbo.UserRolls");
            DropTable("dbo.UserLogins");
            DropTable("dbo.SiteInvites");
            DropTable("dbo.SiteFunctions");
            DropTable("dbo.GlobalOptions");
            DropTable("dbo.DbUsers");
            DropTable("dbo.Connection");
        }
    }
}
