using System.Data.Entity.Validation;
using TheOcdb.Infrastructure;
using TheOcdb.Models;

namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DAL.OCdbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DAL.OCdbContext context)
        {
            #region Global Options
            try
            {
                // Add global options
                context.GlobalOptions.AddOrUpdate(
                    p => p.co_Name,
                    new GlobalOptions
                    {
                        co_Name = "invitation_registration_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "standard_registration_enabled",
                        co_Value = "false",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "initial_invitation_count",
                        co_Value = "0",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "json_article_read_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "download_article_read_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "front_article_search_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "front_article_view_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "blacklist_upload_items_list",
                        co_Value = "exe,emf",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "blacklist_view_items_list",
                        co_Value = "exe,jmf",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "whitelist_upload_items_list",
                        co_Value = "*",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "whitelist_view_items_list",
                        co_Value = "*",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "blacklist_or_whitelist_option",
                        co_Value = "blacklist",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "account_password_change_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "account_username_change_enabled",
                        co_Value = "false",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "account_generate_invite_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "account_login_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "article_create_enabled",
                        co_Value = "true",
                        co_DateChanged = DateTime.Today
                    },
                    new GlobalOptions
                    {
                        co_Name = "site_interest_float",
                        co_Value = "0",
                        co_DateChanged = DateTime.Today
                    });

                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            #endregion

            #region User Roles
            try
            {
                // Add access groups
                context.UserRolls.AddOrUpdate(
                    p => p.ss_AccessName,
                    new UserRolls
                    {
                        ss_AccessLevel = 1,
                        ss_AccessName = "User",
                        ss_Removed = 0
                    },
                     new UserRolls
                     {
                         ss_AccessLevel = 40,
                         ss_AccessName = "VIP",
                         ss_Removed = 0,
                         ss_DerivedID = 1
                     },
                     new UserRolls
                     {
                         ss_AccessLevel = 80,
                         ss_AccessName = "Mod",
                         ss_Removed = 0,
                         ss_DerivedID = 2
                     },
                     new UserRolls
                     {
                         ss_AccessLevel = 99,
                         ss_AccessName = "Admin",
                         ss_Removed = 0,
                         ss_DerivedID = 3
                     });

                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            #endregion

            #region Users
            try
            {
                // Users
                context.DbUsers.AddOrUpdate(
                    p => p.u_LoginName,
                    new DbUsers
                    {
                        u_LoginName = "RetentionNews",
                        u_LoginPassword = PasswordHash.CreateHash("@genT13"),
                        u_DateAdded = DateTime.Today,
                        u_ArticlePassword = PasswordHash.CreateHash("@rT13"),
                        u_Removed = 0,
                        ss_ID = 4,
                        u_RemainingInvites = 5,
                        u_Email = "admin@ocdb.co.za"
                    },
                        new DbUsers
                        {
                            u_LoginName = "Administrator",
                            u_LoginPassword = PasswordHash.CreateHash("@genT13"),
                            u_DateAdded = DateTime.Today,
                            u_ArticlePassword = PasswordHash.CreateHash("@rT13"),
                            u_Removed = 0,
                            ss_ID = 4,
                            u_RemainingInvites = 5,
                            u_Email = "admin@ocdb.co.za"
                        },
                        new DbUsers
                        {
                            u_LoginName = "Test-User",
                            u_LoginPassword = PasswordHash.CreateHash("@genT13"),
                            u_DateAdded = DateTime.Today,
                            u_ArticlePassword = PasswordHash.CreateHash("@rT13"),
                            u_Removed = 0,
                            ss_ID = 1,
                            u_RemainingInvites = 5,
                            u_Email = "admin@ocdb.co.za"
                        },
                        new DbUsers
                        {
                            u_LoginName = "Test-VIP",
                            u_LoginPassword = PasswordHash.CreateHash("@genT13"),
                            u_DateAdded = DateTime.Today,
                            u_ArticlePassword = PasswordHash.CreateHash("@rT13"),
                            u_Removed = 0,
                            ss_ID = 2,
                            u_RemainingInvites = 5,
                            u_Email = "admin@ocdb.co.za"
                        },
                        new DbUsers
                        {
                            u_LoginName = "Test-Mod",
                            u_LoginPassword = PasswordHash.CreateHash("@genT13"),
                            u_DateAdded = DateTime.Today,
                            u_ArticlePassword = PasswordHash.CreateHash("@rT13"),
                            u_Removed = 0,
                            ss_ID = 3,
                            u_RemainingInvites = 5,
                            u_Email = "admin@ocdb.co.za"
                        });
                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            #endregion

            #region Notification Types
            try
            {
                context.TypeNotifications.AddOrUpdate(
                    p => p.tn_Name,
                    new TypeNotifications
                    {
                    tn_Name = "General",
                    tn_IconPath = "icon-bell",
                    tn_IconClass = "label label-sm label-icon label-primary"
                    },
                    new TypeNotifications
                    {
                        tn_Name = "Warning",
                        tn_IconPath = "icon-exclamation",
                        tn_IconClass = "label label-sm label-icon label-warning"
                    },
                    new TypeNotifications
                    {
                        tn_Name = "Error",
                        tn_IconPath = "icon-bolt",
                        tn_IconClass = "label label-sm label-icon label-danger"
                    },
                    new TypeNotifications
                    {
                        tn_Name = "Message",
                        tn_IconPath = "icon-envelope-alt",
                        tn_IconClass = "label label-sm label-icon label-success"
                    },
                    new TypeNotifications
                    {
                        tn_Name = "Clan",
                        tn_IconPath = "icon-sitemap",
                        tn_IconClass = "label label-sm label-icon label-primary"
                    },
                    new TypeNotifications
                    {
                        tn_Name = "Member",
                        tn_IconPath = "icon-user",
                        tn_IconClass = "label label-sm label-icon label-primary"
                    },
                    new TypeNotifications
                    {
                    tn_Name = "Announcement",
                    tn_IconPath = "icon-bullhorn",
                    tn_IconClass = "label label-sm label-icon label-info"
                    },
                    new TypeNotifications
                    {
                        tn_Name = "Request",
                        tn_IconPath = "icon-plus",
                        tn_IconClass = "label label-sm label-icon label-success"
                    });
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            #endregion

            #region User Notifications
            try
            {
                context.UserNotifications.AddOrUpdate(
                    p => p.u_ID, 
                    new UserNotifications
                    {
                        sn_Created = DateTime.Now,
                        sn_Description = "Create a Clan, or join one.",
                        sn_Persistant = true,
                        sn_Read = false,
                        sn_SetIconPath = null,
                        sn_Title = "Create or join a Clan",
                        sn_Type = TypeNotificationE.Clan,
                        u_ID = 2
                    });
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            #endregion

            #region User Mails
            try
            {
                context.UserMail.AddOrUpdate(
                    p => p.um_Subject, new UserMail
                    {
                        um_DateSent = DateTime.Now.AddDays(2),
                        u_ID_From = 1,
                        u_ID_To = 2,
                        um_Message = "This is a test mail",
                        um_MessageRead = 0,
                        um_Removed = 0,
                        um_RemovedDate = null,
                        um_Subject = "Testing Myself"
                                     
                    },new UserMail
                    {
                        um_DateSent = DateTime.Now.AddDays(2),
                        u_ID_From = 3,
                        u_ID_To = 2,
                        um_Message = "Letter from me",
                        um_MessageRead = 0,
                        um_Removed = 0,
                        um_RemovedDate = null,
                        um_Subject = "Is this love?"
                                     
                    });
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            #endregion
        }
    }
}
