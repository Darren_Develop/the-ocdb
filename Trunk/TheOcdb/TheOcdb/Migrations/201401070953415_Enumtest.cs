namespace TheOcdb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Enumtest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SiteNotifications",
                c => new
                    {
                        sn_ID = c.Int(nullable: false, identity: true),
                        sn_TypeID = c.Int(nullable: false),
                        sn_Title = c.String(nullable: false, maxLength: 50, unicode: false),
                        sn_Description = c.String(maxLength: 250, unicode: false),
                        sn_Created = c.DateTime(nullable: false),
                        sn_Read = c.Boolean(nullable: false),
                        sn_SetIconPath = c.String(maxLength: 100, unicode: false),
                        u_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.sn_ID);
            
            CreateTable(
                "dbo.TypeNotifications",
                c => new
                    {
                        tn_ID = c.Int(nullable: false, identity: true),
                        tn_Name = c.String(nullable: false, maxLength: 10, unicode: false),
                        tn_IconPath = c.String(nullable: false, maxLength: 100, unicode: false),
                        tn_IconClass = c.String(maxLength: 500, unicode: false),
                    })
                .PrimaryKey(t => t.tn_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TypeNotifications");
            DropTable("dbo.SiteNotifications");
        }
    }
}
