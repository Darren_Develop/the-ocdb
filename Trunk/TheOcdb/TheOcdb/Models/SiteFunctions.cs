﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace TheOcdb.Models
{
    public class SiteFunctions
    {
        [Key]
        public int sf_ID { get; set; }

        [StringLength(50, ErrorMessage = "Site Function name cannot be longer than 50 characters.", MinimumLength = 1), Required,
         Display(Name = "Site Function Name"), Column(TypeName = "varchar")]
        public string sf_Name { get; set; }

        [Display(Name = "Date Changed")]
        public DateTime sf_DateChanged { get; set; }

    }
}