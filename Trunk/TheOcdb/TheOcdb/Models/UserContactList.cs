﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheOcdb.Models
{
    public class UserContactList
    {
        [Key]
        public int uc_ID { get; set; }

        [DisplayName("Current User ID")]
        public int u_ID_Current { get; set; }

        [DisplayName("Contact User ID")]
        public int? u_ID_Contact { get; set; }

        [StringLength(50, ErrorMessage = "GUID cannot be longer than 50 characters.", MinimumLength = 1), DisplayName("Current User GUID"), Required, Column(TypeName = "varchar")]
        public string u_GUID_Contact { get; set; }

        [StringLength(50, ErrorMessage = "Set Name cannot be longer than 50 characters.", MinimumLength = 1), DisplayName("Set name for contact"), Column(TypeName = "varchar")]
        public string uc_SetName { get; set; }

        [DisplayName("Date Added")]
        public DateTime uc_DateAdded { get; set; }

        [DisplayName("Is Removed"), Range(0, 1)]
        public byte? uc_Removed { get; set; }

        [DisplayName("Date Added")]
        public DateTime? uc_DateRemoved { get; set; }
    }
}