﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheOcdb.Models
{
    public class UserLogins
    {
        [Key]
        public int su_ID { get; set; }

        [StringLength(50, ErrorMessage = "Hash cannot be longer than 50 characters.", MinimumLength = 1), Required,
         Display(Name = "Hash"), DataType(DataType.Text), Column(TypeName = "varchar")]
        public string su_Hash { get; set; }

        [Display(Name = "Date Added"), DataType(DataType.DateTime)]
        public DateTime su_DateAdded { get; set; }

        [Display(Name = "Expiry Added"), DataType(DataType.DateTime)]
        public DateTime su_ExpiryDate { get; set; }

        [Display(Name = "Is Removed"), Range(0, 1)]
        public Byte su_Removed { get; set; }

        [Display(Name = "User ID")]
        public int u_ID { get; set; }

        [ForeignKey("u_ID")]
        public IEnumerable<DbUsers> DbUsers { get; set; }
    }
}