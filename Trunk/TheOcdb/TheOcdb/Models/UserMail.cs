﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheOcdb.Models
{
    public class UserMail
    {
        [NotMapped]
        public string UsernameFrom { get; set; }

        [Key]
        public int um_ID { get; set; }

        [Display(Name = "User Sent From")]
        public int u_ID_From { get; set; }

        [Display(Name = "User Sent To")]
        public int u_ID_To { get; set; }

        [Display(Name = "Date Sent")]
        public DateTime um_DateSent { get; set; }

        [StringLength(50, ErrorMessage = "Subject cannot be longer than 50 characters.", MinimumLength = 1), Display(Name = "Message Subject"), Column(TypeName = "varchar")]
        public string um_Subject { get; set; }

        [StringLength(2000, ErrorMessage = "Message cannot be longer than 2000 characters.", MinimumLength = 1), Display(Name = "Message Content"), Column(TypeName = "varchar")]
        public string um_Message { get; set; }

        [Display(Name = "Is Read"), Range(0, 1)]
        public byte um_MessageRead { get; set; }

        [Display(Name = "Is Removed"), Range(0, 1)]
        public byte? um_Removed { get; set; }

        [Display(Name = "Date Removed")]
        public DateTime? um_RemovedDate { get; set; }
    }
}