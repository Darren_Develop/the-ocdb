﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheOcdb.Models
{
    public class DbUsers
    {
        [Key]
        public int u_ID { get; set; }

        [StringLength(50, ErrorMessage = "Username cannot be longer than 50 characters.", MinimumLength = 1), Required,
         Display(Name = "Login Name"), Column(TypeName = "varchar")]
        public string u_LoginName { get; set; }

        [StringLength(1000, ErrorMessage = "Password cannot be longer than 1000 characters.", MinimumLength = 1)
        , Required, Display(Name = "Password"), DataType(DataType.Password), Column(TypeName = "varchar")]
        public string u_LoginPassword { get; set; }

        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.", MinimumLength = 1)
        , Display(Name = "Name"), Column(TypeName = "varchar")]
        public string u_Name { get; set; }

        [StringLength(50, ErrorMessage = "Surname cannot be longer than 50 characters.", MinimumLength = 1)
        , Display(Name = "Surname"), Column(TypeName = "varchar")]
        public string u_Surname { get; set; }

        [StringLength(50, ErrorMessage = "Email cannot be longer than 50 characters.", MinimumLength = 1)
        , Required, Display(Name = "Email"), Column(TypeName = "varchar")]
        public string u_Email { get; set; }

        [StringLength(250, ErrorMessage = "Profile Image cannot be longer than 250 characters.", MinimumLength = 1)
        , Display(Name = "Profile Image"), Column(TypeName = "varchar")]
        public string u_ProfileImage { get; set; }

        [Display(Name = "Date Added")]
        public DateTime u_DateAdded { get; set; }

        [StringLength(1000, ErrorMessage = "Article Password cannot be longer than 1000 characters.", MinimumLength = 1)
        , Display(Name = "Article Password"), DataType(DataType.Password), Column(TypeName = "varchar")]
        public string u_ArticlePassword { get; set; }

        [Display(Name = "Is Removed"), Range(0, 1)]
        public Byte u_Removed { get; set; }

        [Display(Name = "Remaining Invites")]
        public int u_RemainingInvites { get; set; }

        [Display(Name = "Site Access ID")]
        public int ss_ID { get; set; }
        
        [ForeignKey("ss_ID")]
        public IEnumerable<UserRolls> DbSecuritySite { get; set; }

        public ICollection<Connection> Connections { get; set; }
        public IEnumerator<object> GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }

    public class Connection
    {

        [Key]
        public int conn_ID { get; set; }

        [Column(TypeName = "varchar"), MaxLength(40)]
        public string ConnectionID { get; set; }

        [Column(TypeName = "varchar"), MaxLength(200)]
        public string UserAgent { get; set; }

        public bool Connected { get; set; }
    }
}