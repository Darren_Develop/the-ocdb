﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheOcdb.Models
{
    public class GlobalOptions
    {
        [Key]
        public int co_ID { get; set; }

        [StringLength(100, ErrorMessage = "Option name cannot be longer than 100 characters.", MinimumLength = 1), Required,
         Display(Name = "Global Option Name"), Column(TypeName = "varchar")]
        public string co_Name { get; set; }

        [StringLength(500, ErrorMessage = "Option value cannot be longer than 500 characters.", MinimumLength = 1), Required,
         Display(Name = "Global Option Value"), Column(TypeName = "varchar")]
        public string co_Value { get; set; }

        [Display(Name = "Date Changed")]
        public DateTime co_DateChanged { get; set; }
    }
}
