﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheOcdb.Models
{
    public enum TypeNotificationE
    {
        General = 1,
        Warning,
        Error,
        Message,
        Clan,
        Member,
        Announcement,
        Request
    }

    public class UserNotifications
    {
        [Key]
        public int sn_ID { get; set; }
        
        public TypeNotificationE sn_Type { get; set; }

        [Column(TypeName = "varchar"), StringLength(50, ErrorMessage = "Title name cannot be longer than 50 characters.", MinimumLength = 1), Required]
        public string sn_Title { get; set; }

        [Column(TypeName = "varchar"), StringLength(250, ErrorMessage = "Description name cannot be longer than 250 characters.")]
        public string sn_Description { get; set; }

        [Required]
        public DateTime sn_Created { get; set; }

        public bool sn_Read { get; set; }

        public bool sn_Persistant { get; set; }

        [Column(TypeName = "varchar"), StringLength(100, ErrorMessage = "Icon Path name cannot be longer than 100 characters.")]
        public string sn_SetIconPath { get; set; }

        [Required]
        public int u_ID { get; set; }
    }
}