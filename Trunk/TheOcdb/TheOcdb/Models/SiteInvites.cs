﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace TheOcdb.Models
{
    public class SiteInvites
    {
        [Key]
        public int i_ID { get; set; }

        [Display(Name = "Invite Key"), Required, StringLength(50, ErrorMessage = "Invite Key cannot be longer than 50 characters.", MinimumLength = 1), Column(TypeName = "varchar")]
        public string i_Key { get; set; }

        [Display(Name = "Date Added"), Required]
        public DateTime i_DateAdded { get; set; }

        [Display(Name = "Removed"), Required, Range(0, 1)]
        public Byte i_Removed { get; set; }

        [Display(Name = "Used"), Required, Range(0, 1)]
        public Byte i_Used { get; set; }

        [Display(Name = "Date Used")]
        public DateTime i_DateUsed { get; set; }

        [Display(Name = "ID User Sent"), Required]
        public int u_ID_Sent { get; set; }

        [Display(Name = "ID User Used")]
        public int u_ID_Used { get; set; }
    }
}