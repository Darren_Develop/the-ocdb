﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Drawing.Printing;
using System.Linq;
using System.Web;

namespace TheOcdb.Models
{

    public class TypeNotifications
    {
        [Key]
        public int tn_ID { get; set; }

        [Column(TypeName = "varchar"), StringLength(26, ErrorMessage = "Name name cannot be longer than 10 characters."), Required]
        public string tn_Name { get; set; }

        [Column(TypeName = "varchar"), StringLength(100, ErrorMessage = "Icon Path name cannot be longer than 100 characters."), Required]
        public string tn_IconPath { get; set; }

        [Column(TypeName = "varchar"), StringLength(500, ErrorMessage = "Icon Path name cannot be longer than 500 characters.")]
        public string tn_IconClass { get; set; }
    }
}