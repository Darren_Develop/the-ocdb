﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheOcdb.Models.VM
{
    public class HomeViewModel : ViewModelBase
    {
        public int NewMessages { get; set; }
    }
}