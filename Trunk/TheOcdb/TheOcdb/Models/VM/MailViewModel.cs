﻿using System.Collections.Generic;

namespace TheOcdb.Models.VM
{
    public class MailViewModel : ViewModelBase
    {
        public List<UserMail> UserMails { get; set; }
        public int UserMailCount { get; set; }
    }

    public class GetMessageViewModel : ViewModelBase
    {
        public UserMail UserMail { get; set; }
        public int UserMailCount { get; set; }
    }

    public class ComposeMessageViewModel : ViewModelBase
    {
        public int UserMailCount { get; set; }
        public string MailTo { get; set; }
        public UserMail UserMail { get; set; }
        public List<UserContactList> UserContactList { get; set; }
    }
}