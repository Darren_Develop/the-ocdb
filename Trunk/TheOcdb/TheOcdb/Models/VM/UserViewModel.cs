﻿using System;

namespace TheOcdb.Models.VM
{
    public class UserViewModel : ViewModelBase
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string Nickname { get; set; }
        public string ProfileImage { get; set; }
        public DateTime JoinDate { get; set; }
    }
}