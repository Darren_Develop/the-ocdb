﻿using System.Collections.Generic;
using TheOcdb.Infrastructure;

namespace TheOcdb.Models
{
    public abstract class ViewModelBase
    {
        private List<Noti> _notifications = Infrastructure.Notifications.GetNotifications();
        public List<Noti> Notifications
        {
            get
            {
                return _notifications;
            }
            set
            {
                _notifications = value;
            }
        }

        private string _username = SiteSecurity.GetCurrentUsername();
        public string Username
        {
            get
            {
                return _username;
            }
            set
            {
                _username = value;
            }
        }
    }

    public class Noti : UserNotifications
    {
        public TypeNotifications NotificationType { get; set; }
    }
}