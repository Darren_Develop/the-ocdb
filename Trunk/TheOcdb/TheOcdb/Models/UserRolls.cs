﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TheOcdb.Models
{
    public class UserRolls
    {
        [Key]
        public int ss_ID { get; set; }

        [Display(Name = "Access Level"), Required]
        public int ss_AccessLevel { get; set; }

        [StringLength(50, ErrorMessage = "Access name cannot be longer than 50 characters.", MinimumLength = 1), Display(Name = "Access Name"), Required, Column(TypeName = "varchar")]
        public string ss_AccessName { get; set; }

        [Display(Name = "Derived Id")]
        public int ss_DerivedID { get; set; }

        [Display(Name = "Is Removed"), Range(0, 1)]
        public Byte ss_Removed { get; set; }
    }
}