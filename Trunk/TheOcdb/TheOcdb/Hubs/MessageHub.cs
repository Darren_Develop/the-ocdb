﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using CodeKicker.BBCode;
using Microsoft.AspNet.SignalR;
using TheOcdb.DAL;
using TheOcdb.Models;

namespace TheOcdb.Hubs
{
    public class MessageHub : Hub
    {
        #region Global Initializers
        private enum Mtype
        {
            Cast = 1,
            Other
        }

        private static readonly List<ConnectedUser> ConnectedUsers = new List<ConnectedUser>();
        #endregion

        #region Classes
        public class ConnectedUser
        {
            public int UserId { get; set; }
            public string UserName { get; set; }
            public string ConnectionId { get; set; }
        }
        #endregion

        #region Send Methods
        public void Send(string message)
        {
            // Get the cookie, if one
            HttpCookie myCookie = HttpContext.Current.Request.Cookies[".OCdb.auth"];
            if (myCookie == null) return;

            // Decode it and get the current users name
            FormsAuthenticationTicket ticket = DecodeTicket(myCookie.Value);
            string name = ticket.Name;

            if (string.IsNullOrEmpty(message.Trim()))
            {
                return;
            }

            // Format the message before processing it
            message = message.Replace(Environment.NewLine, "<br />");
            message = message.Replace("\n", Environment.NewLine);
            message = message.Trim();

            if (message.StartsWith("/"))
            {
                _sendCast(name, message);
            }
            else
            {
                _sendMessage(name, message);
            }
        }
        
        private void _sendMessage(string name, string message)
        {
            string curTime = DateTime.Now.ToString("T");

            using (var db = new OCdbContext())
            {
                DbUsers user = db.DbUsers.Single(x => x.u_LoginName == name);
                if (user == null)
                {
                    // User not found
                    return;
                }

                db.Entry(user)
                    .Collection(u => u.Connections)
                    .Query()
                    .Where(c => c.Connected)
                    .Load();

                if (user.Connections == null)
                {
                    // User no longer connected
                    return;
                }

                foreach (Connection connection in user.Connections)
                {
                    string currentPMessage = message;
                    Mtype pType = FormatMessage(ref currentPMessage, "You");
                    if (pType == Mtype.Cast)
                    {
                        Clients.Client(connection.ConnectionID).sendCast(curTime, CustomBbParse(BBCode.ToHtml(currentPMessage)));
                    }
                    else
                    {
                        Clients.Client(connection.ConnectionID).sendMessage(curTime, name, CustomBbParse(BBCode.ToHtml(currentPMessage)));
                    }
                }
                string currentMessage = message;
                Mtype type = FormatMessage(ref currentMessage, name);
                if (type == Mtype.Cast)
                {
                    Clients.AllExcept(user.Connections.Select(x => x.ConnectionID).ToArray()).sendCast(curTime, CustomBbParse(BBCode.ToHtml(currentMessage)));
                }
                else
                {
                    Clients.AllExcept(user.Connections.Select(x => x.ConnectionID).ToArray()).sendMessage(curTime, name, CustomBbParse(BBCode.ToHtml(currentMessage)));
                }
            }
        }

        private void _sendCast(string name, string message)
        {
            string curTime = DateTime.Now.ToString("T");

            string splitted = message.Split(' ')[0];

            switch (splitted)
            {
                case "/me":
                    message = name + " " + message.Replace((splitted + " "), string.Empty);
                    break;
                case "/slap":
                    message = "[cast]" + name + " slaps " + message.Split(' ')[1] + "!";
                    break;
                case "/bow":
                    message = "[cast]" + name + " humbly bows down to " + message.Split(' ')[1] + ".";
                    break;
                case "/shock":
                    message = "[cast]" + name + " is shocked by the last message!";
                    break;
                case "/call":
                    //SendMail(message.Split(' ')[1]);
                    return;
                default:
                    if (splitted == "/msg" || splitted == "/pm" || splitted == "/send")
                    {
                        _sendPrivateMessage(message.Replace((splitted + " "), string.Empty).Replace(message.Split(' ')[1] + " ", string.Empty), name, message.Split(' ')[1]);
                        return;
                    }
                    Clients.Caller.sendPrivateMessage(curTime, CustomBbParse("[cast]Invalid command."));
                    break;
            }

            Clients.All.sendCast(curTime, CustomBbParse(message));
        }

        private void _sendPrivateMessage(string message, string from = "", string toId = "")
        {

            string curTime = DateTime.Now.ToString("T");

            if (string.IsNullOrEmpty(from) && string.IsNullOrEmpty(toId))
            {
                var curUser = GetClientId();
                Clients.Client(curUser).sendPrivateMessage(curTime, CustomBbParse("[imporatant]" + message));
            }
            else
            {
                // Sent to client
                Clients.Clients(GetConntectionIdList(toId)).sendPrivateMessage(curTime, CustomBbParse("[imporatant][privatemessage] (from: " + from + ")[/privatemessage] " + message));

                // Confirmation from client
                Clients.Caller.sendPrivateMessage(curTime, CustomBbParse("[imporatant][privatemessage] (to: " + toId + ")[/privatemessage] " + message));
            }


        }

        private static Mtype FormatMessage(ref string message, string name)
        {
            Mtype returning;
            bool me = (name == "You");
            switch (message.ToLower())
            {
                case "lol":
                    message = String.Format("[cast] {0} laugh{1}.", name, (me) ? "" : "s");
                    returning = Mtype.Cast;
                    break;
                case "rofl":
                    message = String.Format("[cast] {0} roll{1} on the floor laughing.", name, (me) ? "" : "s");
                    returning = Mtype.Cast;
                    break;
                default:
                    returning = Mtype.Other;
                    break;
            }

            return returning;
        }
        #endregion

        #region Custom Methods
        /// <summary>
        /// Get's the currently connected Id of the client.
        /// This is unique for each client and is used to identify
        /// a connection.
        /// </summary>
        /// <returns>The client Id.</returns>
        private string GetClientId()
        {
            string clientId = "";
            if (Context.QueryString["clientId"] != null)
            {
                // clientId passed from application 
                clientId = Context.QueryString["clientId"];
            }

            if (string.IsNullOrEmpty(clientId.Trim()))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }

        /// <summary>
        /// Get all the connection ID's of the supplied username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        private static List<string> GetConntectionIdList(string username)
        {
            return ConnectedUsers.Where(x => x.UserName == username).Select(x => x.ConnectionId).ToList();
        }

        /// <summary>
        /// Gets a list of all the current online users
        /// </summary>
        /// <returns></returns>
        private void GetCurrentOnlineList()
        {
            Clients.All.getOnlineList(ConnectedUsers.Select(x => x.UserName).Distinct().ToList());
        }
        
        public string CustomBbParse(string message)
        {
            var parser = new BBCodeParser(new[]
                {
                    new BBTag("cast", "<span style=\"color:#ff8c00\">", "</span>"),
                    new BBTag("imporatant", "<span style=\"color:#FFF;font-weight:bold\">", "</span>"),
                    new BBTag("span", "<span>", "</span>"),
                    new BBTag("privatemessage", "<span class=\"label label-important\">", "</span>"),
                    new BBTag("header", "<strong style=\"color: greenyellow\">", "</strong>") 
                });
            return parser.ToHtml(message);
        }

        [Obsolete]
        public void SendMail(string to)
        {
            MailMessage myMailMessage = new MailMessage();

            try
            {
                myMailMessage.From = new MailAddress("chat@ocdb.com");
                myMailMessage.To.Add(to);
                myMailMessage.Subject = "OCDB Chat Call";
                myMailMessage.IsBodyHtml = true;
                myMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                myMailMessage.Body = String.Format("<div style=\"width:600px\"><p style=\"text-align:left\">Hey there!<br><br>You've been called by someone to join the <strong>OCDB</strong>'s online chat!<br>This free service is totally anonymous, unrestricted, uncensored, and unmonitored!<br><br>&nbsp;To accept, <a title=\"The Online Chat and Data Broadcaster\" href=\"http://ocdb.co.za/\" target=\"_blank\">click here</a>.</p><hr size=\"1\" style=\"width:600px\"></div>");

                SmtpClient smtpServer = new SmtpClient("winmail01.hkdns.co.za", 25)
                {
                    UseDefaultCredentials = false,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    Credentials = new NetworkCredential("chat@ocdb.com", "!dentifyM3")
                };
                smtpServer.Send(myMailMessage);

                _sendPrivateMessage("Call successfully sent.");
            }
            catch
            {
                _sendPrivateMessage("Call unsuccessful.");
            }
        }
        
        public FormsAuthenticationTicket DecodeTicket(string encodedTicket)
        {
            return FormsAuthentication.Decrypt(encodedTicket);
        }
        #endregion

        #region Connect Methods
        /// <summary>
        /// The OnConnected event.
        /// </summary>
        /// <returns>
        /// </returns>
        [Authorize]
        public override Task OnConnected()
        {
            string connectionId = Context.ConnectionId;
            string curTime = DateTime.Now.ToString("T");

            try
            {
                HttpCookie myCookie = HttpContext.Current.Request.Cookies.Get(".OCdb.auth");

                if (myCookie != null)
                {
                    FormsAuthenticationTicket ticket = DecodeTicket(myCookie.Value);
                    string name = ticket.Name;


                    using (var db = new OCdbContext())
                    {
                        DbUsers user =
                            db.DbUsers.Include(u => u.Connections).SingleOrDefault(u => u.u_LoginName == name);

                        if (user != null)
                        {
                            user.Connections.Add(new Connection
                            {
                                ConnectionID = connectionId,
                                UserAgent = Context.Request.Headers["User-Agent"],
                                Connected = true
                            });
                            db.SaveChanges();

                            ConnectedUsers.Add(new ConnectedUser
                            {
                                UserId = user.u_ID,
                                UserName = user.u_LoginName,
                                ConnectionId = connectionId
                            });

                            int connCount = ConnectedUsers.Count(x => x.UserId == user.u_ID);
                            if (connCount == 1)
                            {
                                Clients.Caller.setHeader(CustomBbParse("[header]Welcome to the chat!"));
                                Clients.AllExcept(user.Connections.Select(x => x.ConnectionID).ToArray()).getUserState(curTime, name + " joined.");
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

            }

            GetCurrentOnlineList();

            return base.OnConnected();
        }
        
        /// <summary>
        /// The OnDisconnected event.
        /// </summary>
        /// <returns>
        /// </returns>
        public override Task OnDisconnected()
        {
            string curTime = DateTime.Now.ToString("T");
            string connId = Context.ConnectionId;

            using (var db = new OCdbContext())
            {
                Connection connection = db.Connection.FirstOrDefault(x => x.ConnectionID == connId);
                ConnectedUser user =
                    ConnectedUsers.FirstOrDefault(x => x.ConnectionId == connId);

                if (connection != null)
                {
                    connection.Connected = false;
                    db.SaveChanges();

                    if (ConnectedUsers.Any())
                    {
                        ConnectedUsers.RemoveAll(x => x.ConnectionId == Context.ConnectionId);
                        bool has = ConnectedUsers.Any(x => user != null && x.UserId == user.UserId);
                        if (!has)
                        {
                            if (user != null) Clients.All.getUserState(curTime, user.UserName + " left.");
                        }
                    }
                }
            }

            GetCurrentOnlineList();

            return base.OnDisconnected();
        }

        #endregion
    }
}