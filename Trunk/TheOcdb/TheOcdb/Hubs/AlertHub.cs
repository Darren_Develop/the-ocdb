﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace TheOcdb.Hubs
{
    public class AlertHub : Hub
    {
        public void SendAlert(string connId)
        {
            Clients.Client(connId).sendAlert();
        }
    }
}