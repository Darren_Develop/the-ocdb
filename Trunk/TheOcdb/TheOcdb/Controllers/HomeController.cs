﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TheOcdb.DAL;
using TheOcdb.Infrastructure;
using TheOcdb.Models;
using TheOcdb.Models.VM;

namespace TheOcdb.Controllers
{
    [Auth(Roles = "User")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            int curUser = SiteSecurity.GetCurrentUserId();
            using (OCdbContext context = new OCdbContext())
            {
                HomeViewModel viewModel = new HomeViewModel
                {
                    NewMessages = context.UserMail.Count(x => x.u_ID_To == curUser && x.um_MessageRead == 0 && x.um_Removed != 1)
                };

                return View(viewModel);
            }
        }


        public ActionResult Update()
        {
            return View();
        }

        public ActionResult Chat()
        {
            try
            {
                HttpCookie myCookie = Request.Cookies[".OCdb.auth"];

                if (myCookie != null)
                {
                    FormsAuthenticationTicket ticket = DecodeTicket(myCookie.Value);
                    var name = ticket.Name;
                    using (var db = new OCdbContext())
                    {
                        var user = db.DbUsers
                            .Include(u => u.Connections)
                            .SingleOrDefault(u => u.u_LoginName == name);

                        TempData["lin"] = true;

                    }
                }
            }
            catch (Exception)
            {
                TempData["lin"] = false;
            }

            ChatViewModel viewModel = new ChatViewModel
            {
            };

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Chat(string displayname, string password)
        {
            using (OCdbContext context = new OCdbContext())
            {
                DbUsers firstOrDefault = context.DbUsers.FirstOrDefault(p => p.u_LoginPassword == password);
                if (firstOrDefault != null)
                {
                    string usLer = firstOrDefault.u_LoginName;

                    if (usLer == null) return RedirectToAction("Chat");

                    if (usLer != displayname) return RedirectToAction("Chat");

                    FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, displayname, DateTime.Now,
                        DateTime.Now.AddDays(2), false, "SecChat Data");


                    Response.Cookies.Add(new HttpCookie(".OCdb.auth", EncodeTicket(ticket)));

                    TempData["lin"] = true;
                    TempData["displayname"] = displayname;
                    return RedirectToAction("Chat");
                }
            }
            return RedirectToAction("Chat");

        }

        public string EncodeTicket(FormsAuthenticationTicket ticket)
        {
            return FormsAuthentication.Encrypt(ticket);
        }

        public FormsAuthenticationTicket DecodeTicket(string encodedTicket)
        {
            return FormsAuthentication.Decrypt(encodedTicket);
        }
    }
}