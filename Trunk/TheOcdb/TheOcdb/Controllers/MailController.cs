﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using TheOcdb.DAL;
using TheOcdb.Infrastructure;
using TheOcdb.Models;
using TheOcdb.Models.VM;

namespace TheOcdb.Controllers
{
    [Auth(Roles = "User")]
    public class MailController : Controller
    {
        public ActionResult Index()
        {
            using (OCdbContext _context = new OCdbContext())
            {
                List<UserMail> returnMail = new List<UserMail>();
                int curUser = SiteSecurity.GetCurrentUserId();
                try
                {
                    List<UserMail> userMail =
                        _context.UserMail.OrderByDescending(u => u.um_ID)
                            .Where(x => x.u_ID_To == curUser && (x.um_Removed == null || x.um_Removed == 0))
                            .ToList();

                    foreach (UserMail mail in userMail)
                    {
                        UserMail innerTemp = mail;
                        innerTemp.UsernameFrom = GetContactName(mail.u_ID_From);

                        returnMail.Add(innerTemp);
                    }
                }
                catch (Exception)
                {
                    returnMail = new List<UserMail>();
                }

                MailViewModel mvm = new MailViewModel
                {
                    UserMails = returnMail,
                    UserMailCount = GetUnreadMailCount()
                };

                return View(mvm);
            }
        }

        public ActionResult Message(string id)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                int messageId = Int32.Parse(id);
                UserMail userMail;
                int curUser = SiteSecurity.GetCurrentUserId();

                try
                {
                    userMail = _context.UserMail.Single(x => x.um_ID == messageId);

                    if (userMail.u_ID_To == curUser) // Make sure the email belongs to the user.
                    {
                        userMail.UsernameFrom = GetContactName(userMail.u_ID_From);

                        userMail.um_Message = userMail.um_Message.Trim();

                        // Mark the mail as read
                        if (userMail.um_MessageRead == 0)
                        {
                            userMail.um_MessageRead = 1;
                            _context.Entry(userMail).State = EntityState.Modified;
                            _context.SaveChanges();
                        }
                    }
                    else
                        return RedirectToAction("Index", "Mail");
                }
                catch (Exception)
                {
                    return RedirectToAction("Index", "Mail");
                }

                GetMessageViewModel gmvm = new GetMessageViewModel()
                {
                    UserMail = userMail,
                    UserMailCount = GetUnreadMailCount()
                };

                return View(gmvm);
            }
        }

        public ActionResult Compose()
        {
            ComposeMessageViewModel cmvm = new ComposeMessageViewModel
            {
                UserMailCount = GetUnreadMailCount()
            };

            return View(cmvm);
        }

        public ActionResult RemoveMessage(string messageIdString)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                string[] messageIds = messageIdString.Split(',');
                int curUser = SiteSecurity.GetCurrentUserId();

                int successCount = 0;
                int failCount = 0;

                foreach (string messageId in messageIds)
                {
                    if (!string.IsNullOrEmpty(messageId))
                    {
                        UserMail umail;
                        try
                        {
                            umail = _context.UserMail.Find(Int32.Parse(messageId));
                        }
                        catch (Exception)
                        {
                            failCount++;
                            continue;
                        }

                        if (umail.u_ID_To == curUser) // Make sure the email belongs to the user.
                        {
                            try
                            {
                                umail.um_Removed = 1;
                                umail.um_RemovedDate = DateTime.Now;
                                _context.Entry(umail).State = EntityState.Modified;
                                _context.SaveChanges();

                                successCount++;
                            }
                            catch (Exception)
                            {
                                failCount++;
                            }
                        }
                        else
                        {
                            failCount++;
                        }
                    }
                }

                return Json("Messaged Deleted: " + successCount + " - Failed: " + failCount);
            }
        }

        public ActionResult MarkRead(string messageIdString)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                string[] messageIds = messageIdString.Split(',');
                int curUser = SiteSecurity.GetCurrentUserId();

                int successCount = 0;
                int failCount = 0;

                foreach (string messageId in messageIds)
                {
                    if (!string.IsNullOrEmpty(messageId))
                    {
                        UserMail umail;
                        try
                        {
                            umail = _context.UserMail.Find(Int32.Parse(messageId));
                        }
                        catch (Exception)
                        {
                            failCount++;
                            continue;
                        }

                        if (umail.u_ID_To == curUser) // Make sure the email belongs to the user.
                        {
                            try
                            {
                                // Mark the mail as read
                                if (umail.um_MessageRead == 0)
                                {
                                    umail.um_MessageRead = 1;
                                    _context.Entry(umail).State = EntityState.Modified;
                                    _context.SaveChanges();
                                }

                                successCount++;
                            }
                            catch (Exception)
                            {
                                failCount++;
                            }
                        }
                        else
                        {
                            failCount++;
                        }
                    }
                }

                return Json("Messaged Read: " + successCount + " - Failed: " + failCount);
            }
        }

        public ActionResult MarkUnread(string messageIdString)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                string[] messageIds = messageIdString.Split(',');
                int curUser = SiteSecurity.GetCurrentUserId();

                int successCount = 0;
                int failCount = 0;

                foreach (string messageId in messageIds)
                {
                    if (!string.IsNullOrEmpty(messageId))
                    {
                        UserMail umail;
                        try
                        {
                            umail = _context.UserMail.Find(Int32.Parse(messageId));
                        }
                        catch (Exception)
                        {
                            failCount++;
                            continue;
                        }

                        if (umail.u_ID_To == curUser) // Make sure the email belongs to the user.
                        {
                            try
                            {
                                // Mark the mail as read
                                if (umail.um_MessageRead == 1)
                                {
                                    umail.um_MessageRead = 0;
                                    _context.Entry(umail).State = EntityState.Modified;
                                    _context.SaveChanges();
                                }

                                successCount++;
                            }
                            catch (Exception)
                            {
                                failCount++;
                            }
                        }
                        else
                        {
                            failCount++;
                        }
                    }
                }

                return Json("Messaged Unread: " + successCount + " - Failed: " + failCount);
            }
        }

        public string GetContactName(int id)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                int curUser = SiteSecurity.GetCurrentUserId();
                string contactName = string.Empty;

                // Check if user has member in contact list
                try
                {
                    contactName = _context.UserContactList.SingleOrDefault(x => x.u_ID_Contact == id && x.u_ID_Current == curUser).uc_SetName;
                }
                catch
                {
                }

                if (string.IsNullOrEmpty(contactName))
                {
                    contactName = _context.DbUsers.SingleOrDefault(x => x.u_ID == id).u_LoginName;
                }


                return contactName;

            }
        }

        public int GetUnreadMailCount(int userId)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                return _context.UserMail.Count(x => x.u_ID_To == userId && x.um_MessageRead == 0 && x.um_Removed == 0);
            }
        }

        public int GetUnreadMailCount()
        {
            int curUser = SiteSecurity.GetCurrentUserId();
            using (OCdbContext _context = new OCdbContext())
            {
                return _context.UserMail.Count(x => x.u_ID_To == curUser && x.um_MessageRead == 0 && x.um_Removed != 1);
            }
        }

        public ActionResult SendMessage(string to, string subject, string message)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                int id = SiteSecurity.GetCurrentUserId();
                int toId = SiteSecurity.FindUser(to);

                if (toId == 0)
                {
                    return null;
                }

                UserMail um = new UserMail
                {
                    u_ID_From = id,
                    u_ID_To = toId,
                    um_DateSent = DateTime.Now,
                    um_Message = message.Trim(),
                    um_MessageRead = 0,
                    um_Subject = subject
                };

                _context.UserMail.Add(um);
                _context.SaveChanges();

                return null;
            }
        }
    }
}