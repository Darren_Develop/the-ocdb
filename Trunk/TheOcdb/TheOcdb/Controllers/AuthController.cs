﻿using System;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web.Mvc;
using TheOcdb.DAL;
using TheOcdb.Infrastructure;
using TheOcdb.Models;

namespace TheOcdb.Controllers
{
    public class AuthController : Controller
    {
        public ActionResult Index()
        {
            return View("Login");
        }

        #region Login Results
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string username, string password, int? remember)
        {
            bool loginSuccess = SiteSecurity.SetLogin(username, password, remember.HasValue);

            if (loginSuccess)
            {
                return RedirectToAction("Index", "Home");
            }

            TempData["username"] = username;
            TempData["error"] = "Username / Password invalid.";
            return RedirectToAction("Login");
        }
        #endregion

        #region Logout Results

        public ActionResult Logout()
        {
            SiteSecurity.SetLogout();
            return RedirectToAction("Index");
        }
        #endregion

        #region Forgot Password Results

        [HttpPost]
        public ActionResult ForgotPassword(string email)
        {
            return RedirectToAction("Index", new { sent = true });
        }
        #endregion

        #region Register Results
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(string fullname, string email, string username, string password)
        {
            using (OCdbContext _context = new OCdbContext())
            {
                bool inviteReg =
                    Parsing.FullParse(
                        _context.GlobalOptions.Single(x => x.co_Name == "invitation_registration_enabled").co_Value);

                bool standardReg =
                    Parsing.FullParse(
                        _context.GlobalOptions.Single(x => x.co_Name == "standard_registration_enabled").co_Value);

                if (!inviteReg && !standardReg) return RedirectToAction("Register");

                // Check if username is taken
                if (SiteSecurity.CheckUsername(username))
                {
                    return RedirectToAction("Register", new { ret = "Username already taken" });
                }

                string firstname;
                string lastname = " ";
                // Get firstname and lastname variables from fullname
                if (fullname.Split(' ').Count() > 1)
                {
                    firstname = fullname.Split(new[] { ' ', ' ' }, 2, StringSplitOptions.None)[0].Trim();
                    lastname = fullname.Split(new[] { ' ', ' ' }, 2, StringSplitOptions.None)[1].TrimStart();
                }
                else
                {
                    firstname = fullname;
                }

                // Add User
                DbUsers createdUser = new DbUsers
                {
                    u_LoginName = username,
                    u_LoginPassword = PasswordHash.CreateHash(password),
                    u_DateAdded = DateTime.Now,
                    u_ArticlePassword = PasswordHash.CreateHash(password),
                    u_Removed = 0,
                    u_RemainingInvites =
                        Parsing.IntParse(
                            _context.GlobalOptions.Single(x => x.co_Name == "initial_invitation_count").co_Value),
                    ss_ID = 1,
                    u_Name = firstname,
                    u_Surname = lastname,
                    u_Email = email
                };

                try
                {
                    _context.DbUsers.Add(createdUser);
                    _context.SaveChanges();
                }
                catch (Exception)
                {
                    return RedirectToAction("Register");
                }

                int takenId = createdUser.u_ID;

                try
                {
                    //if (!string.IsNullOrEmpty(RouteData.Values["id"].ToString()) && RouteData.Values["id"] != null)
                    //{
                    //    string key = RouteData.Values["id"].ToString();

                    //    // Set the invite to taken
                    //    SiteInvites siteInvites = _context.SiteInvites.Single(x => x.i_Key == key);
                    //    siteInvites.i_DateUsed = DateTime.Now;
                    //    siteInvites.i_Used = 1;
                    //    siteInvites.u_ID_Used = takenId;

                    //    _context.Entry(siteInvites).State = EntityState.Modified;
                    //    _context.SaveChanges();
                    //}

                    UserNotifications note = new UserNotifications
                    {
                        sn_Created = DateTime.Now,
                        sn_Description = "Create a Clan, or join one.",
                        sn_Persistant = true,
                        sn_Read = false,
                        sn_SetIconPath = null,
                        sn_Title = "Create or join a Clan",
                        sn_Type = TypeNotificationE.Clan,
                        u_ID = takenId
                    };

                    _context.UserNotifications.Add(note);
                    _context.SaveChanges();

                    UserMail mail = new UserMail
                    {
                        um_DateSent = DateTime.Now.AddDays(2),
                        u_ID_From = 2,
                        u_ID_To = takenId,
                        um_Message = String.Format("&lt;p&gt; &lt;strong&gt;Welcome to the OCdb!&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;This site is dedicated to creating maintaining a " +
                                     "platform for any gamer or clan to use. We intent to host full features at no cost, to give the gaming community what it needs!&amp;nbsp;&lt;em&gt;(Pretty vague, eh?)&lt;/em&gt;&lt;/p&gt;" +
                                     "&lt;p&gt;The site is currently still in pre-alpha, and the only reason you could&amp;#39;ve gotten here is through word of mouth! " +
                                     "Please&amp;nbsp;&lt;u&gt;don&amp;#39;t&lt;/u&gt; distribute the link, as there really isn&amp;#39;t much to show yet..&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;&lt;p&gt;Feel free to message me (Administrator) with any comments to info!&lt;/p&gt;"),
                        um_MessageRead = 0,
                        um_Removed = 0,
                        um_RemovedDate = null,
                        um_Subject = "Welcome!"
                    };

                    _context.UserMail.Add(mail);
                    _context.SaveChanges();

                }
                catch (DbEntityValidationException e)
                {
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                            eve.Entry.Entity.GetType().Name, eve.Entry.State);
                        foreach (var ve in eve.ValidationErrors)
                        {
                            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                                ve.PropertyName, ve.ErrorMessage);
                        }
                    }
                    throw;
                }

                //Log the current user in
                SiteSecurity.SetLogin(username, password, true);

                return RedirectToAction("Index", "Home");
            }
        }
        #endregion

        #region Data Checks
        public ActionResult CheckUsername(string username)
        {
            //return SiteSecurity.FindUsername(username) ? Json("false, a", JsonRequestBehavior.AllowGet) : Json("true", JsonRequestBehavior.AllowGet);
            return SiteSecurity.CheckUsername(username) ? Content("false") : Content("true");
        }

        public ActionResult CheckEmail(string email)
        {
            return SiteSecurity.CheckEmail(email) ? Content("false") : Content("true");
        }
        #endregion
    }
}