﻿using System.Linq;
using System.Web.Mvc;
using System;
using TheOcdb.DAL;
using TheOcdb.Infrastructure;
using TheOcdb.Models.VM;

namespace TheOcdb.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        public ActionResult View(string id)
        {
            using (OCdbContext context = new OCdbContext())
            {
                UserViewModel uvm = new UserViewModel
                {
                    Notifications = Notifications.GetNotifications()
                };
                var curUser = context.DbUsers.Where(x => x.u_LoginName == id && x.u_Removed != 1).Select(x => new { x.u_Name, x.u_DateAdded, x.u_Email, x.u_Surname, x.u_ProfileImage}).FirstOrDefault();
                
                if (curUser == null)
                {
                    return View(uvm);
                }

                uvm.Name = curUser.u_Name;
                uvm.Surname = curUser.u_Surname;
                uvm.Email = curUser.u_Email;
                uvm.JoinDate = curUser.u_DateAdded;
                uvm.Nickname = id;
                uvm.ProfileImage = String.IsNullOrEmpty(curUser.u_ProfileImage) ? "~/assets/img/profile/default.jpg" : curUser.u_ProfileImage;
                
                return View(uvm);
            }
        }

        public ActionResult UpdateImage(string imageUrl)
        {
            using (OCdbContext context = new OCdbContext())
            {
                var user = context.DbUsers.FirstOrDefault(z => z.u_ID == SiteSecurity.GetCurrentUserId());
                user.u_ProfileImage = imageUrl;
                context.Entry(user);

            }
            return Json("Success", JsonRequestBehavior.AllowGet);
        }
    }
}