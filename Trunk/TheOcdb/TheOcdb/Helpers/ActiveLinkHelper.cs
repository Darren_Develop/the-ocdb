﻿using System;
using System.Web.Mvc;

namespace TheOcdb.Helpers
{
    public static class ActiveLinkHelper
    {
        public static MvcHtmlString MenuItem(this HtmlHelper htmlHelper,string action)
        {
            string currentAction = (string)htmlHelper.ViewContext.RouteData.Values["action"];
            string after;

            if (string.Equals(
                currentAction,
                action,
                StringComparison.CurrentCultureIgnoreCase)
                )
            {
                after = "<li class=\"start active\">";
            }
            else
            {
                after =  "<li class=\"\">";
            }
            
            return MvcHtmlString.Create(after);
        }
    }

}