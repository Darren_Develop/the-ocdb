﻿using System;
using System.Web.Mvc;
using TheOcdb.PartialRequests;

namespace TheOcdb.Helpers
{
    public static class PartialRequestHelper
    {
        public static void RenderPartialRequest(this HtmlHelper html, string viewDataKey)
        {
            PartialRequest partial = html.ViewContext.ViewData.Eval(viewDataKey) as PartialRequest;
            if (partial != null)
                partial.Invoke(html.ViewContext);
        }
    }
}