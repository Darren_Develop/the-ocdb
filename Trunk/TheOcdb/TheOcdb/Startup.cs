﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;
using TheOcdb;

[assembly: OwinStartup(typeof(Startup))]
namespace TheOcdb
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR("/controlR", new HubConfiguration());
        }
    }
}