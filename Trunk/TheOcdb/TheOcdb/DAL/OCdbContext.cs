﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using TheOcdb.Models;

namespace TheOcdb.DAL
{
    public class OCdbContext : DbContext
    {
        //public DbSet<FileTypeExt> FileTypeExt { get; set; }

        public DbSet<DbUsers> DbUsers { get; set; }

        public DbSet<UserLogins> UserLogins { get; set; }

        public DbSet<UserRolls> UserRolls { get; set; }

        public DbSet<SiteInvites> SiteInvites { get; set; }

        public DbSet<SiteFunctions> SiteFunctions { get; set; }

        public DbSet<GlobalOptions> GlobalOptions { get; set; }

        //public DbSet<TakedownRequests> TakedownRequests { get; set; }

        public DbSet<Connection> Connection { get; set; }

        public DbSet<UserNotifications> UserNotifications { get; set; }

        public DbSet<TypeNotifications> TypeNotifications { get; set; }

        public DbSet<UserMail> UserMail { get; set; }

        public DbSet<UserContactList> UserContactList { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}