﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace TheOcdb.Infrastructure
{
    public class Parsing
    {
        public static bool FullParse(string stringToParse)
        {
            Boolean parsedValue;
            return Boolean.TryParse(stringToParse, out parsedValue) && parsedValue;
        }

        public static int IntParse(string stringToParse)
        {
            int outCount;

            if (Int32.TryParse(stringToParse, out outCount))
            {
                return outCount;
            }
            else
            {
                return 0;
            }
        }
    }

    public class Base64
    {
        #region Encoding
        /// <summary>
        /// Encode a string to Base64
        /// </summary>
        /// <param name="stringToEncode">String that will be encoded</param>
        /// <returns>Base64 encoded string</returns>
        public static string StringEncode(string stringToEncode)
        {
            byte[] asciiBytes = Encoding.ASCII.GetBytes(stringToEncode);
            return Convert.ToBase64String(asciiBytes);
        }

        /// <summary>
        /// Encode and Encrypt a string to Base64 using an Encryption Key
        /// </summary>
        /// <param name="stringToEncode">String that will be encoded</param>
        /// <param name="encryptionKey">Key used to encrypt the string</param>
        /// <returns>Encrypted Base64 encoded string</returns>
        [Obsolete]
        public static string StringEncode(string stringToEncode, string encryptionKey)
        {
            //byte[] encryptedString = EncryptString(stringToEncode, StringTo128(encryptionKey));
            byte[] encryptedString = Encoding.ASCII.GetBytes(Encrypt(stringToEncode, encryptionKey));
            return Convert.ToBase64String(encryptedString);
        }

        /// <summary>
        /// Encode a byte to Base64
        /// </summary>
        /// <param name="byteToEncode">Byte that will be encoded</param>
        /// <returns>Base64 encoded string</returns>
        public static string ByteEncode(byte[] byteToEncode)
        {
            return Convert.ToBase64String(byteToEncode);
        }
        #endregion

        #region Decoding
        /// <summary>
        /// Decode a string from Base64
        /// </summary>
        /// <param name="stringToDecode">Base64 string that will be decoded</param>
        /// <returns>Decoded string</returns>
        public static string StringDecode(string stringToDecode)
        {
            byte[] decodedString = Convert.FromBase64String(stringToDecode);
            return Encoding.UTF8.GetString(decodedString);
        }

        /// <summary>
        /// Decode a string from Base64
        /// </summary>
        /// <param name="stringToDecode">Base64 string that will be decoded to byte array</param>
        /// <returns>Decoded string</returns>
        public static byte[] StringDecodeToByte(string stringToDecode)
        {
            byte[] decodedString = Convert.FromBase64String(stringToDecode);
            return decodedString;
        }

        /// <summary>
        /// Decode and Decrypt a string from Base64 using an Encryption Key
        /// </summary>
        /// <param name="stringToDecode">Base64 string that will be decoded</param>
        /// <param name="encryptionKey">Key used to encrypt the string</param>
        /// <returns>Encrypted Base64 encoded string</returns>
        [Obsolete]
        public static string StringDecode(string stringToDecode, string encryptionKey)
        {
            //[] b64 = Convert.FromBase64String(stringToDecode);
            /*
             * to b64
             * encrypt
             * 
             * decrypt
             * from b64
             */
            byte[] key128 = GetBytes(encryptionKey);

            byte[] decryptedToB64 = Encoding.ASCII.GetBytes(Decrypt(stringToDecode, encryptionKey));

            return GetString(decryptedToB64);
            //return GetString(DecryptBytesToBytes(GetBytes(StringDecode(stringToDecode)), key128));
            //return StringDecode(GetString(DecryptBytesToBytes(GetBytes(stringToDecode), key128)));
        }
        #endregion

        #region Encryption Test
        private static byte[] EncryptString(string toEncrypt, byte[] encryptionKey)
        {
            var toEncryptBytes = Encoding.UTF8.GetBytes(toEncrypt);
            using (var provider = new AesCryptoServiceProvider())
            {
                provider.Key = encryptionKey;
                provider.Mode = CipherMode.CBC;
                provider.Padding = PaddingMode.PKCS7;
                using (var encryptor = provider.CreateEncryptor(provider.Key, provider.IV))
                {
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                        {
                            cs.Write(toEncryptBytes, 0, toEncryptBytes.Length);
                            cs.FlushFinalBlock();
                            var retVal = new byte[16 + ms.Length];
                            provider.IV.CopyTo(retVal, 0);
                            ms.ToArray().CopyTo(retVal, 16);
                            return retVal;
                        }
                    }
                }
            }
        }

        static byte[] DecryptBytesToBytes(byte[] cipherText, byte[] Key)
        {
            // Check arguments. 
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold 
            // the decrypted text.
            byte[] encrypted = null;

            // Create an AesCryptoServiceProvider object 
            // with the specified key and IV. 
            using (AesCryptoServiceProvider aesAlg = new AesCryptoServiceProvider())
            {
                aesAlg.Key = Key;
                aesAlg.IV = Key;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption. 
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            string encrypted_text = srDecrypt.ReadToEnd();
                            encrypted = new byte[encrypted_text.Length * sizeof(char)];
                            System.Buffer.BlockCopy(encrypted_text.ToCharArray(), 0, encrypted, 0, encrypted.Length);
                        }
                    }
                }

            }

            return encrypted;
        }

        private static byte[] DecryptString(byte[] encryptedString, byte[] encryptionKey)
        {
            using (var provider = new AesCryptoServiceProvider())
            {
                provider.Key = encryptionKey;
                provider.Mode = CipherMode.CBC;
                provider.Padding = PaddingMode.PKCS7;
                provider.IV = encryptedString.Take(16).ToArray();
                using (var ms = new MemoryStream(encryptedString, 16, encryptedString.Length - 16))
                {
                    using (var decryptor = provider.CreateDecryptor(provider.Key, provider.IV))
                    {
                        using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                        {
                            //using (StreamReader srDecrypt = new StreamReader(cs))
                            //{
                            //    string encrypted_text = srDecrypt.ReadToEnd();
                            //    byte[] encrypted = new byte[encrypted_text.Length * sizeof(char)];
                            //    Buffer.BlockCopy(encrypted_text.ToCharArray(), 0, encrypted, 0, encrypted.Length);

                            //    return encrypted;
                            //}

                            byte[] decrypted = new byte[encryptedString.Length];
                            var byteCount = cs.Read(decrypted, 0, encryptedString.Length);
                            return decrypted;
                        }
                    }
                }
            }
        }



        public static string EncryptDecrypt(string value, string key)
        {
            string encrypted = Encrypt(value, key);
            string decrypted = Decrypt(encrypted, key);

            return decrypted;
            //return ED(value,key);
        }

        private static string ED(string value, string key)
        {
            int Rfc2898KeygenIterations = 100;
            int AesKeySizeInBits = 128;
            String Password = key;
            byte[] Salt = StringTo128(key);
            byte[] rawPlaintext = Encoding.ASCII.GetBytes(value);
            byte[] cipherText;
            byte[] plainText;
            using (Aes aes = new AesManaged())
            {
                aes.Padding = PaddingMode.PKCS7;
                aes.KeySize = AesKeySizeInBits;
                int KeyStrengthInBytes = aes.KeySize / 8;
                Rfc2898DeriveBytes rfc2898 =
                    new Rfc2898DeriveBytes(Password, Salt, Rfc2898KeygenIterations);
                aes.Key = rfc2898.GetBytes(KeyStrengthInBytes);
                aes.IV = rfc2898.GetBytes(KeyStrengthInBytes);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(rawPlaintext, 0, rawPlaintext.Length);
                    }
                    cipherText = ms.ToArray();
                }

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherText, 0, cipherText.Length);
                    }
                    plainText = ms.ToArray();
                }
            }
            string s = Encoding.ASCII.GetString(plainText);
            return s;
        }

        private static string Encrypt(string toEncrypt, string encryptionKey)
        {
            int Rfc2898KeygenIterations = 100;
            int AesKeySizeInBits = 128;
            String Password = encryptionKey;
            byte[] Salt = StringTo128(encryptionKey);
            byte[] rawPlaintext = Encoding.ASCII.GetBytes(toEncrypt);
            byte[] cipherText;
            byte[] plainText;
            using (Aes aes = new AesManaged())
            {
                aes.Padding = PaddingMode.PKCS7;
                aes.KeySize = AesKeySizeInBits;
                int KeyStrengthInBytes = aes.KeySize / 8;
                Rfc2898DeriveBytes rfc2898 =
                    new Rfc2898DeriveBytes(Password, Salt, Rfc2898KeygenIterations);
                aes.Key = rfc2898.GetBytes(KeyStrengthInBytes);
                aes.IV = rfc2898.GetBytes(KeyStrengthInBytes);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(rawPlaintext, 0, rawPlaintext.Length);
                    }
                    cipherText = ms.ToArray();
                }
            }
            string s = Encoding.ASCII.GetString(cipherText);
            return s;
        }

        private static string Decrypt(string toDecrypt, string decryptionKey)
        {
            int Rfc2898KeygenIterations = 100;
            int AesKeySizeInBits = 128;
            String Password = decryptionKey;
            byte[] Salt = StringTo128(decryptionKey);
            byte[] plainText;
            using (Aes aes = new AesManaged())
            {
                aes.Padding = PaddingMode.PKCS7;
                aes.KeySize = AesKeySizeInBits;
                int KeyStrengthInBytes = aes.KeySize / 8;
                Rfc2898DeriveBytes rfc2898 =
                    new Rfc2898DeriveBytes(Password, Salt, Rfc2898KeygenIterations);
                aes.Key = rfc2898.GetBytes(KeyStrengthInBytes);
                aes.IV = rfc2898.GetBytes(KeyStrengthInBytes);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(Encoding.ASCII.GetBytes(toDecrypt), 0, toDecrypt.Length);
                    }
                    plainText = ms.ToArray();
                }
            }
            string s = Encoding.ASCII.GetString(plainText);
            return s;
        }
        #endregion

        #region String Manipulation
        private static byte[] StringTo128(string stringToConvert)
        {
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < 8; i++)
            {
                b.Append(stringToConvert[i % stringToConvert.Length]);
            }
            stringToConvert = b.ToString();
            byte[] key = Encoding.ASCII.GetBytes(stringToConvert);//key size is 16 bytes = 128 bits

            return key;
        }

        public static string StringTo128String(string stringToConvert)
        {
            StringBuilder b = new StringBuilder();
            for (int i = 0; i < 8; i++)
            {
                b.Append(stringToConvert[i % stringToConvert.Length]);
            }
            stringToConvert = b.ToString();
            byte[] key = Encoding.ASCII.GetBytes(stringToConvert);//key size is 16 bytes = 128 bits

            return GetString(key);
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static string GetString(byte[] bytes)
        {
            char[] chars = new char[bytes.Length / sizeof(char)];
            System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
            return new string(chars);
        }
        #endregion
    }

    public class PluralFormatProvider : IFormatProvider, ICustomFormatter
    {

        public object GetFormat(Type formatType)
        {
            return this;
        }


        public string Format(string format, object arg, IFormatProvider formatProvider)
        {
            string[] forms = format.Split(';');
            int value = (int)arg;
            int form = value == 1 ? 0 : 1;
            return forms[form];
        }

    }
}