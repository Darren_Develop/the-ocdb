﻿using System.Collections.Generic;
using System.Linq;
using TheOcdb.DAL;
using TheOcdb.Models;

namespace TheOcdb.Infrastructure
{
    public static class Notifications
    {
        public static List<Noti> GetNotifications()
        {
            int curUserId = SiteSecurity.GetCurrentUserId();
            using (OCdbContext context = new OCdbContext())
            {
                List<UserNotifications> a = context.UserNotifications.Where(x => x.u_ID == curUserId).ToList();
                return a.Select(note => new Noti
                 {
                     NotificationType = GetNotificationType(note.sn_Type),
                     sn_Created = note.sn_Created,
                     sn_Description = note.sn_Description,
                     u_ID = note.u_ID,
                     sn_ID = note.sn_ID,
                     sn_Persistant = note.sn_Persistant,
                     sn_Read = note.sn_Read,
                     sn_SetIconPath = note.sn_SetIconPath,
                     sn_Title = note.sn_Title,
                     sn_Type = note.sn_Type
                 }).ToList();
            }
        }

        private static TypeNotifications GetNotificationType(TypeNotificationE type)
        {
            using (OCdbContext context = new OCdbContext())
            {
                return context.TypeNotifications.SingleOrDefault(x => x.tn_ID == (int)type);
            }
        }
    }
}