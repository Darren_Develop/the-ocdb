﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using TheOcdb.DAL;
using TheOcdb.Models;

namespace TheOcdb.Infrastructure
{
    public class SiteSecurity
    {
        // The following constants may be changed without breaking existing hashes.
        public const int SaltByteSize = 200;
        public const int HashByteSize = 1000;
        public const int Pbkdf2Iterations = 1000;

        public const int IterationIndex = 0;
        public const int SaltIndex = 1;
        public const int Pbkdf2Index = 2;

        private static readonly OCdbContext Context = new OCdbContext();

        /// <summary>
        /// Revokes any site access for the logged in user
        /// </summary>
        public static void SetLogout()
        {
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[".OCdb.auth"];
            if (httpCookie == null) return;

            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(httpCookie.Value);
            string cookieHash = ticket.UserData;

            httpCookie.Expires = DateTime.Today.AddDays(-5);
            HttpContext.Current.Response.AppendCookie(httpCookie);

            UserLogins userLogin = Context.UserLogins.Single(x => x.su_Hash == cookieHash);

            userLogin.su_Removed = 1;

            Context.Entry(userLogin).State = EntityState.Modified;
            Context.SaveChanges();
        }

        /// <summary>
        /// Gets the current logged in users ID
        /// </summary>
        /// <returns>User ID</returns>
        public static int GetCurrentUserId()
        {
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[".OCdb.auth"];
            if (httpCookie == null) return 0;

            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(httpCookie.Value); ;
            var name = ticket.Name;
            using (var db = new OCdbContext())
            {
                return db.DbUsers.SingleOrDefault(u => u.u_LoginName == name).u_ID;
            }
        }

        public static string GetCurrentUsername()
        {
            HttpCookie httpCookie = HttpContext.Current.Request.Cookies[".OCdb.auth"];
            if (httpCookie == null) return null;

            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(httpCookie.Value); ;
            return ticket.Name;
        }

        public static bool CheckUsername(string username)
        {
            try
            {
                DbUsers userTemp = Context.DbUsers.Single(x => x.u_LoginName == username);

                return userTemp != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CheckEmail(string email)
        {
            try
            {
                DbUsers userTemp = Context.DbUsers.FirstOrDefault(x => x.u_Email == email);

                return userTemp != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static string FindUser(int id)
        {
            return Context.DbUsers.Single(x => x.u_ID == id).u_LoginName;
        }

        public static int FindUser(string username)
        {
            return string.IsNullOrEmpty(username) ? 0 : Context.DbUsers.FirstOrDefault(x => x.u_LoginName == username).u_ID;
        }

        public static bool SetLogin(string username, string password, bool remembered)
        {
            string guid = Guid.NewGuid().ToString();
            DbUsers curUser = Context.DbUsers.FirstOrDefault(u => u.u_LoginName == username);

            if (curUser != null && PasswordHash.ValidatePassword(password, curUser.u_LoginPassword))
            {
                UserLogins currentLogin = new UserLogins
                {
                    su_DateAdded = DateTime.Now,
                    su_ExpiryDate = remembered ? DateTime.Now.AddDays(14) : DateTime.Now.AddMinutes(20),
                    su_Hash = guid,
                    u_ID = curUser.u_ID,
                    su_Removed = 0
                };
                Context.UserLogins.Add(currentLogin);
                Context.SaveChanges();

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                    1,
                    username,
                    DateTime.Now,
                    remembered ? DateTime.Now.AddDays(14) : DateTime.Now.AddMinutes(20),
                    true,
                    guid,
                    FormsAuthentication.FormsCookiePath
                    );

                HttpContext.Current.Response.Cookies.Add(new HttpCookie(".OCdb.auth", FormsAuthentication.Encrypt(ticket)));

                return true;
            }
            return false;
        }
    }

    public class Auth : AuthorizeAttribute
    {
        /// <summary>
        /// Checks whether the user's current login state is valid
        /// </summary>
        /// <returns>true or false</returns>
        public static bool CheckUserLogin()
        {
            using (OCdbContext Context = new OCdbContext())
            {
                HttpCookie httpCookie = HttpContext.Current.Request.Cookies[".OCdb.auth"];
                if (httpCookie == null) return false;

                try
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(httpCookie.Value);

                    UserLogins userLoginsTemp = Context.UserLogins.FirstOrDefault(x => x.su_Hash == ticket.UserData);
                    if (userLoginsTemp == null || (userLoginsTemp.u_ID == 0))
                    {
                        return false;
                    }

                    if ((userLoginsTemp.su_DateAdded < DateTime.Now) && (userLoginsTemp.su_ExpiryDate > DateTime.Now))
                    {
                        return true;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
                return false;
            }
        }

        /// <summary>
        /// Check if a user is in the roll provided, or a parent roll.
        /// </summary>
        /// <param name="accessName">Name of roll</param>
        /// <returns>true or false</returns>
        public static bool CheckRoll(string accessName)
        {
            using (OCdbContext Context = new OCdbContext())
            {
                HttpCookie httpCookie = HttpContext.Current.Request.Cookies[".OCdb.auth"];
                if (httpCookie != null)
                {
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(httpCookie.Value);
                    try
                    {
                        UserLogins currentLogin = Context.UserLogins.FirstOrDefault(x => x.su_Hash == ticket.UserData);
                        DbUsers currentUser;
                        UserRolls userRolls;

                        try
                        {
                            // Get the roll details where the roll name = accessName
                            userRolls = Context.UserRolls.Single(x => x.ss_AccessName == accessName);
                        }
                        catch (Exception)
                        {
                            // If the developer types in the roll name wrong
                            return false;
                        }
                        try
                        {
                            // Get the current user details where the user ID = current logged in user ID
                            currentUser = Context.DbUsers.Single(x => x.u_ID == currentLogin.u_ID);
                        }
                        catch (Exception)
                        {
                            // If the user connected to the hash cant be found
                            return false;
                        }


                        //Check if user in the group
                        if (currentUser.ss_ID == userRolls.ss_ID)
                        {
                            return true;
                        }

                        // Get all current user rolls
                        UserRolls currentUserRoll = Context.UserRolls.Single(x => x.ss_ID == currentUser.ss_ID);

                        // Return whether the current user roll's access level is higher than the pages requested access level
                        return (currentUserRoll.ss_AccessLevel >= userRolls.ss_AccessLevel &&
                                currentUserRoll.ss_AccessLevel > 0);
                    }
                    catch (Exception)
                    {
                        // If the hash fails;
                        return false;
                    }
                }

                return false;
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return CheckUserLogin() && CheckRoll(Roles);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(
                new RouteValueDictionary
                        {
                            {"action", "Login"},
                            {"controller", "Auth"}
                        });
        }
    }
}
